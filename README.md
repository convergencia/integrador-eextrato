# Integrador e-Extrato

## Instruções para baixar do repositório
Baixe esse projeto normalmente e abra na IDE como projeto Maven.
O Maven realizará as configurações necessárias para que o projeto funcione.

## Instruções para executar da própria IDE
Execute a classe io.convergencia.eextrato.EExtrato como um aplicativo java comum

Acesse a url: http://localhost:8090/integrador-eextrato




## Componentes/frameworks utilizados

###Back-end
####Springframework 
* spring-boot-starter v1.4.2.RELEASE
* spring-boot-starter-security v1.4.2.RELEASE 
** removido spring-web, spring-context, spring-beans
* spring-security-oauth v2.0.2.RELEASE 
** removido spring-web, spring-context, spring-beans, spring-security-core
* spring-security-web v4.2.0.RELEASE
* spring-boot-starter-data-jpa v1.4.2.RELEASE 
** removido spring-beans
* spring-boot-starter-data-rest v1.4.2.RELEASE 
** removido spring-beans
* spring-orm v1.4.2.RELEASE 
** removido spring-beans		

####RestExpress
* RestExpress v0.11.3

####SpringFox
* springfox-swagger-ui v2.2.2 
** removido spring-hateoas
* springfox-swagger2 v2.2.2 
** removido spring-hateoas, spring-beans

####Hibernate
* hibernate-core v5.0.11.Final

####JDBC
* postgresql v9.4.1212.jre7
* mssql-jdbc v6.1.0.jre7

###front-end
* rdash-ui v1.0.*
* angular v~1.5.0
* angular-cookies v~1.5.0
* angular-bootstrap v~1.0.3
* angular-ui-router v^0.2.15
* bootstrap v~3.3.6
* font-awesome v^4.5.0
* angular-resource v^1.5.9
* moment v^2.17.0
* angularjs-datepicker v^2.1.7
* angular-advanced-searchbox v^3.0.0
* angular-i18n v^1.6.0
* ngstorage v^0.3.11
