/* global moment */

/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash')
        .controller('VendaPndEnvioCtrl', ['$scope', 'VendaService', 'LojaService', VendaPndEnvioCtrl]);


function VendaPndEnvioCtrl($scope, VendaService, LojaService) {

    self = this;
    self.vendas = [];
    self.paramConsulta = {};
    self.paramDefault = {};
    self.currentPage = 1;

    self.init = function () {

        self.paramDefault.redes = new EnumOperadora().list();
        self.paramConsulta.redes = [];
        self.paramDefault.bandeiras = new EnumBandeira().list();
        self.paramConsulta.bandeiras = [];
        self.paramDefault.filiais = LojaService.query({query: '{status:1}'});
        self.paramConsulta.codigoLojas = [];
        self.paramConsulta.periodoIni = '';
        self.paramConsulta.periodoFim = '';
        self.paramConsulta.vendaIni = '';
        self.paramConsulta.vendaFim = '';
        VendaService.status(function (res) {
            console.log(res);
            self.paramDefault.status = res;
            
        });
    };


    self.searchVendas = function () {
        //TODO incluir validacao dos dados
        self.vendas = [];
        var p = angular.copy(self.paramConsulta);
        if (p.periodoIni) {
            p.periodoIni = moment(p.periodoIni.split("/").reverse().join("-")).toISOString();
        }

        if (p.periodoFim) {
            p.periodoFim = moment(p.periodoFim.split("/").reverse().join("-")).toISOString();
        }

        p.bandeiras = p.bandeiras.map(function (itm) {
            return itm.index;
        });

        p.redes = p.redes.map(function (itm) {
            return itm.index;
        });

        p.codigoLojas = p.codigoLojas.map(function (itm) {
            return itm.codigoLoja;
        });

        console.log(p);

        VendaService.search(JSON.stringify(p), function (result) {
            self.vendas = result;
            self.vendas.forEach(function (i) {
                i.dataVenda = new Date(i.dataVenda);
            });
        }, function (err) {
            console.info(err);
        });
    };

    self.init();

}

//http://localhost:8080/esb/boavista/venda