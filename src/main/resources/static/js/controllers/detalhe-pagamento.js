
angular.module('RDash').controller('DetalhePagamentoCtrl', ['$stateParams', 'RetornoPagamentoService', DetalhePagamentoCtrl]);


function DetalhePagamentoCtrl($stateParams, RetornoPagamentoService) {

    var self = this;
    self.p = {};

    self.loadPagamento = function () {
        RetornoPagamentoService.get({id: $stateParams.id}, function (r) {
            self.p = r.data;
        });
    };

    self.loadPagamento();
}