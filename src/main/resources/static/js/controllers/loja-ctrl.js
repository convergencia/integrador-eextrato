angular.module('RDash')
        .controller('LojaCtrl', ['$scope', 'LojaService'/*, '$uibModal', 'ModalInstanceCtrl'*/, LojaCtrl]);


function LojaCtrl($scope, LojaService/*, $uibModal, ModalInstanceCtrl*/) {

    var self = this;
    self.filialSelecionada = new LojaService();
    self.filais = [];

    self.init = function () {
        LojaService.query(function (data) {
            self.filais = data;
        });
    };

    self.save = function () {
        self.filialSelecionada.$save(function (data) {
            self.filais.push(data);
            self.filialSelecionada = new LojaService();
        });
    };

    self.remove = function (item) {
        LojaService.delete({id: item.id}, function () {
            self.init();
        });
        
    };

    self.askDelete = function (item) {

        if (window.confirm("Deseja relamente excluir item ?")) {
            self.remove(item);
        }
//        
//        var message = "Deseja relamente excluir item ?";
//
//        var modalHtml = '<div class="modal-body">' + message + '</div>';
//        modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';
//
//        var modalInstance = $uibModal.open({
//            template: modalHtml,
//            controller: ModalInstanceCtrl
//        });
//
//        modalInstance.result.then(function () {
//            self.remove(item);
//        });
    };

    self.select = function (it) {
        self.filialSelecionada = it;
    };

    self.init();

}