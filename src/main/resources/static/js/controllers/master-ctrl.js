/**
 * Master Controller
 */

angular.module('RDash')
        .controller('MasterCtrl', ['$rootScope', '$state', '$scope', '$cookieStore', 'UserService', MasterCtrl]);

function MasterCtrl($rootScope, $state, $scope, $cookieStore, UserService) {

    var main = this;
    function logout() {
        main.currentUser = UserService.setCurrentUser(null);
        $state.go('login');
    }
    $rootScope.$on('authorized', function () {
        main.currentUser = UserService.getCurrentUser();
    });
    $rootScope.$on('unauthorized', function () {
        main.currentUser = UserService.setCurrentUser(null);
        $state.go('login');
    });
    main.logout = logout;
    main.currentUser = UserService.getCurrentUser();


    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;

    $scope.getWidth = function () {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function (newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = !$cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function () {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function () {
        $scope.$apply();
    };
}