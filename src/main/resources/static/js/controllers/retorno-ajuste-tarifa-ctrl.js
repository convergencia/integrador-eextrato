/* global moment */

/**
 * Created by Wanderson on 01/12/2016.
 */

angular.module('RDash')
        .controller('RetornoAjusteCtrl', ['$scope', 'LojaService', 'RetornoAjusteTarifaService', RetornoAjusteCtrl]);


function RetornoAjusteCtrl($scope, LojaService, RetornoAjusteTarifaService) {

    var self = this;
    self.paramConsulta = {};
    self.paramDefault = {};
    self.tarifas = [];
    self.currentPage = 1;

    self.init = function () {
        self.paramDefault.redes = new EnumOperadora().list();
        self.paramConsulta.redes = [];
        self.paramDefault.bandeiras = new EnumBandeira().list();
        self.paramConsulta.bandeiras = [];
        self.paramDefault.filiais = LojaService.query({query: '{status:1}'});
        self.paramConsulta.codigoLojas = [];
        self.paramConsulta.periodoIni = '';
        self.paramConsulta.periodoFim = '';
        self.paramConsulta.loteIni = '';
        self.paramConsulta.loteFim = '';

    };

    self.searchTarifas = function () {
        //TODO incluir validacao dos dados
        self.tarifas = [];
        var p = angular.copy(self.paramConsulta);
        if (p.periodoIni) {
            p.periodoIni = moment(p.periodoIni.split("/").reverse().join("-")).toISOString();
        }

        if (p.periodoFim) {
            p.periodoFim = moment(p.periodoFim.split("/").reverse().join("-")).toISOString();
        }

        p.bandeiras = p.bandeiras.map(function (itm) {
            return itm.index;
        });

        p.redes = p.redes.map(function (itm) {
            return itm.index;
        });

        p.codigoLojas = p.codigoLojas.map(function (itm) {
            return itm.codigoLoja;
        });

        RetornoAjusteTarifaService.search(p, function (result) {
            self.tarifas = result;
            self.tarifas.forEach(function (i) {
                i.dataVenda = new Date(i.dataVenda);
                i.dataCreditoDebito = new Date(i.dataCreditoDebito);
            });
        }, function (err) {
            console.info(err);
        });
    };

    self.init();

}
