angular.module('RDash')
        .controller('DashboardCtrl', ['$scope', 'ErroService', 'RetornoAjusteTarifaService', 'RetornoPagamentoService', 'RetornoVendaService', 'VendaService' , DashboardCtrl]);


function DashboardCtrl ($scope, ErroService, RetornoAjusteTarifaService, RetornoPagamentoService, RetornoVendaService, VendaService){
    
    var self = this;
    
    self.ajusteRetorno = 0;
    self.pagamentoRetorno = 0;
    self.vendaRetorno = 0;
    self.vendaEnvio = 0;
    self.vendaErro = 0;

    self.init = function () {
        ErroService.ultimos(function (data) {
            self.resultado = data;
        });

        VendaService.countEnviados(function (data) {
            self.vendaRetorno = data.count;
        });

        VendaService.countErros(function (data) {
            self.vendaErro = data.count;
        });

        RetornoPagamentoService.count(function (data) {
            self.pagamentoRetorno = data.count;
        });

        RetornoAjusteTarifaService.count(function (data) {
            self.ajusteRetorno = data.count;
        });

        VendaService.countPendentes(function (data) {
            self.vendaEnvio = data.count;
        });
        
    };
    
    self.init();
}