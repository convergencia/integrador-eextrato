/* global moment */

/**
 * Created by Wanderson on 02/12/2016.
 */

angular.module('RDash')
    .controller('RetornoPagamentoCtrl', ['$scope', 'LojaService', 'RetornoPagamentoService', RetornoPagamentoCtrl]);


function RetornoPagamentoCtrl($scope, LojaService, RetornoPagamentoService) {

    var self = this;
    self.paramConsulta = {};
    self.paramDefault = {};
    self.pagamentos = [];
    self.currentPage = 1;

    self.init = function () {
        self.paramDefault.redes = new EnumOperadora().list();
        self.paramConsulta.redes = [];
        self.paramDefault.bandeiras = new EnumBandeira().list();
        self.paramConsulta.bandeiras = [];
        self.paramDefault.filiais = LojaService.query({query: '{status:1}'});
        self.paramConsulta.codigoLojas = [];
        self.paramConsulta.periodoIni = '';
        self.paramConsulta.periodoFim = '';
        self.paramConsulta.loteIni = '';
        self.paramConsulta.loteFim = '';

    };

    self.searchPagamentos = function () {
        //TODO incluir validacao dos dados

        

        self.pagamentos = [];
        var p = angular.copy(self.paramConsulta);
        if(p.periodoIni){
            p.periodoIni = moment(p.periodoIni.split("/").reverse().join("-")).toISOString();
        }

        if(p.periodoFim){
            p.periodoFim = moment(p.periodoFim.split("/").reverse().join("-")).toISOString();
        }

        p.bandeiras = p.bandeiras.map(function (itm) {
            return itm.index;
        });

        p.redes = p.redes.map(function (itm) {
            return itm.index;
        });

        p.codigoLojas = p.codigoLojas.map(function (itm) {
            return itm.codigoLoja;
        });

        RetornoPagamentoService.search(p, function (result) {
            self.pagamentos = result;
            self.pagamentos.forEach(function (i) {
                i.dataVenda = new Date(i.dataVenda);
                i.dataCredito = new Date(i.dataCredito);
            });
        }, function (err) {
            console.info(err);
        });
    };

    self.init();

}

