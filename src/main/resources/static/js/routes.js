

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash').config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        'use strict';
        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
                .state('index', {
                    data: {pageTitle: 'Home', breadcrumb: 'Home'},
                    url: '/',
                    templateUrl: 'templates/dashboard.html'
                })
                .state('vendaPendenteEnvio', {
                    data: {pageTitle: 'Envio venda', breadcrumb: 'Home / Envio / Venda'},
                    url: '/venda-pendente-envio',
                    templateUrl: 'templates/venda-pendente-envio.html'
                })
                .state('retornoAjusteTarifa', {
                    data: {pageTitle: 'Retorno Ajuste e Tarifa', breadcrumb: 'Home / Retorno / Ajuste e Tarifa'},
                    url: '/retorno-ajuste-tarifa',
                    templateUrl: 'templates/retorno-ajuste-tarifa.html'
                })
                .state('retornoVenda', {
                    data: {pageTitle: 'Retorno Venda', breadcrumb: 'Home / Retorno / Venda'},
                    url: '/retorno-venda',
                    templateUrl: 'templates/retorno-venda.html'
                })
                .state('retornoPagamento', {
                    data: {pageTitle: 'Retorno Pagamento', breadcrumb: 'Home / Retorno / Pagamento'},
                    url: '/retorno-pagamento',
                    templateUrl: 'templates/retorno-pagamento.html'
                })
                .state('log', {
                    url: '/log',
                    templateUrl: 'templates/log.html'
                })
                .state('usuario', {
                    url: '/usuario',
                    templateUrl: 'templates/cadastro-usuario.html'
                })
                .state('loja', {
                    data: {pageTitle: 'Lojas', breadcrumb: 'Home / Cadastro / Loja'},
                    url: '/loja',
                    templateUrl: 'templates/cadastro-loja.html'
                })
                .state('login', {
                    url: '/login',
                    data: {pageTitle: 'Login', breadcrumb: 'login'},
                    templateUrl: 'templates/login.html'
                })
                .state('detalhe-pagamento', {
                    url: '/detalhe-pagemento/:id',
                    templateUrl: 'templates/detalhe-pagamento.html'
                });                

        $httpProvider.interceptors.push('SecurityInterceptor');
    }
]);

angular.module('RDash').run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }]);