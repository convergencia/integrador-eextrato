/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash').factory('RetornoAjusteTarifaService', ['$resource', RetornoAjusteTarifaService]);

function RetornoAjusteTarifaService($resource) {

    return $resource('api/retorno-ajuste-tarifa/:id', {
        query: '@id'
    }, {
        search: {
            method: 'POST',
            cache: false,
            url: 'api/retorno-ajuste-tarifa/search',
            isArray: true
        },
        count: {
            method: 'GET',
            cache: false,
            url: 'api/retorno-ajuste-tarifa/count',
            isArray: false
        }
    });
}