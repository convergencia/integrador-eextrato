/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash').factory('LojaService', ['$resource', LojaService]);

function LojaService($resource) {

    return $resource('api/loja/:id', {
        query: '@id'
    }, {
        count: {
            method: 'GET',
            cache: false,
            url: 'api/loja/count',
            isArray: false
        }
    });
}