angular.module('RDash').factory('ErroService', ['$resource', ErroService]);

function ErroService($resource) {

    return $resource('api/erro-sistema/:id', {
        query: '@id'
    }, {
        search: {
            method: 'POST',
            cache: false,
            url: 'api/erro-sistema/search',
            isArray: true
        },
        ultimos: {
            method: 'GET',
            cache: false,
            url: 'api/erro-sistema/ultimos',
            isArray: true
        },
        count: {
            method: 'GET',
            cache: false,
            url: 'api/erro-sistema/count',
            isArray: false
        }
    });
}


