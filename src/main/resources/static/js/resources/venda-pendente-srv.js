/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash').factory('VendaService', ['$resource', VendaService]);

function VendaService($resource) {

    return $resource('api/envio-venda/:id', {
        query: '@id'
    }, {
        search: {
            method: 'POST',
            cache: false,
            url: 'api/envio-venda/search',
            isArray: true
        },
        countPendentes: {
            method: 'GET',
            cache: false,
            url: 'api/envio-venda/count/pendentes',
            isArray: false
        },
        countEnviados: {
            method: 'GET',
            cache: false,
            url: 'api/envio-venda/count/enviados',
            isArray: false
        },
        countErros: {
            method: 'GET',
            cache: false,
            url: 'api/envio-venda/count/erros',
            isArray: false
        },
        status: {
            method: 'GET',
            cache: false,
            url: 'api/envio-venda/status',
            isArray: true
        }
    });
}