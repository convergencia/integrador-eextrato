/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash').factory('RetornoPagamentoService', ['$resource', RetornoPagamentoService]);

function RetornoPagamentoService($resource) {

    return $resource('api/retorno-pagamento/:id', {
        query: '@id'
    }, {
        search: {
            method: 'POST',
            cache: false,
            url : 'api/retorno-pagamento/search',
            isArray: true
        },
        count: {
            method: 'GET',
            cache: false,
            url: 'api/retorno-pagamento/count',
            isArray: false
        }
    });
}