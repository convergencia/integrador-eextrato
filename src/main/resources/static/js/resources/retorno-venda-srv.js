/**
 * Created by Wanderson on 28/11/2016.
 */

angular.module('RDash').factory('RetornoVendaService', ['$resource', RetornoVendaService]);

function RetornoVendaService($resource) {

    return $resource('api/retorno-venda/:id', {
        query: '@id'
    }, {
        search: {
            method: 'POST',
            cache: false,
            url: 'api/retorno-venda/search',
            isArray: true
        },
        count: {
            method: 'GET',
            cache: false,
            url: 'api/retorno-venda/count',
            isArray: false
        }
    });
}