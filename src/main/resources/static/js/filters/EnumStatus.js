/**
 * Created by Wanderson on 02/12/2016.
 */
function EnumStatus(index, label) {
    this.index = index;
    this.label = label;
}

EnumStatus.CONSOLIDADA = new EnumStatus(1, 'Consoliada');
EnumStatus.NAO_CONSOLIDADA = new EnumStatus(2, 'Não Consoliada');


EnumStatus.prototype.getById = function (id) {
    switch (id) {
        case  1 :
            return EnumStatus.CONSOLIDADA;
        case  2 :
            return EnumStatus.NAO_CONSOLIDADA;
    }
};

EnumStatus.prototype.list = function () {
    var a = [EnumStatus.CONSOLIDADA, EnumStatus.NAO_CONSOLIDADA];

    return a;
};