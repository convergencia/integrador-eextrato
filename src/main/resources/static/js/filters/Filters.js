angular.module('RDash').filter('operadora', Operadora);

function Operadora() {
    return function (input) {
        return new EnumOperadora().getById(input).label;
    };
}

angular.module('RDash').filter('bandeira', Bandeira);

function Bandeira() {
    return function (input) {
        return new EnumBandeira().getById(input).label;
    };
}

angular.module('RDash').filter('statusvenda', StatusVenda);

function StatusVenda() {
    return function (input) {
        return new EnumStatus().getById(input).label;
    };
}

angular.module('RDash').filter('historico', HistoricoVenda);

function HistoricoVenda() {
    return function (input) {
        return new EnumHistorico().getById(input).label;
    };
}