function EnumOperadora(index, label){
    this.index = index;
    this.label = label;
}

EnumOperadora.UNDEFINED  = new EnumOperadora(0, 'Indefinido');
EnumOperadora.AMEX = new EnumOperadora(1, 'Amex');
EnumOperadora.CIELO = new EnumOperadora(2, 'Cielo');
EnumOperadora.CREDISHOP = new EnumOperadora(3, 'CrediShop');
EnumOperadora.FORTBRASIL = new EnumOperadora(4, 'FortBrasil');
EnumOperadora.HIPERCARD = new EnumOperadora(5, 'HiperCard');
EnumOperadora.REDECARD = new EnumOperadora(6, 'RedeCard');
EnumOperadora.SANTANDER = new EnumOperadora(7, 'Santander');
EnumOperadora.GOODCARD = new EnumOperadora(8, 'GoodCard');
EnumOperadora.LIBERCARD = new EnumOperadora(9, 'LiberCard');
EnumOperadora.ELAVON = new EnumOperadora(10, 'Elavon');
EnumOperadora.BANESECARD = new EnumOperadora(11, 'BaneseCard');
EnumOperadora.TICKET = new EnumOperadora(12, 'Ticket');
EnumOperadora.FIRSTDATA = new EnumOperadora(13, 'FirstData');
EnumOperadora.AVISTA = new EnumOperadora(14, 'Avista');
EnumOperadora.TRIBANCO = new EnumOperadora(15, 'Tribanco');
EnumOperadora.CETELEM = new EnumOperadora(16, 'Cetelém');
EnumOperadora.SODEXO = new EnumOperadora(17, 'Sodexo');
EnumOperadora.VRBENEFICIOS = new EnumOperadora(18, 'VRBenefícios');
EnumOperadora.PROTEGECARD = new EnumOperadora(19, 'Protegecard');
EnumOperadora.VERO = new EnumOperadora(20, 'Vero');
EnumOperadora.LOSANGO = new EnumOperadora(21, 'Losango');
EnumOperadora.DACASA = new EnumOperadora(22, 'DaCasa');
EnumOperadora.POLICARD = new EnumOperadora(23, 'Policard');
EnumOperadora.VALECARD = new EnumOperadora(24, 'Valecard');
EnumOperadora.COMPROCARD = new EnumOperadora(25, 'Comprocard');
EnumOperadora.FANCARD = new EnumOperadora(26, 'Fancard');
EnumOperadora.ECXCARD = new EnumOperadora(27, 'Ecxcard');
EnumOperadora.BANESCARD = new EnumOperadora(28, 'Banescard');
EnumOperadora.CABAL = new EnumOperadora(29, 'Cabal');
EnumOperadora.USECRED = new EnumOperadora(30, 'Usecred');
EnumOperadora.PLANVALE = new EnumOperadora(32, 'Planvale');
EnumOperadora.ABRAPETITE = new EnumOperadora(33, 'Abrapetite');
EnumOperadora.SENF = new EnumOperadora(34, 'Senf');


EnumOperadora.prototype.getById = function (id) {
    switch (id) {
    	case 0 : 
    		return EnumOperadora.UNDEFINED;
        case  1 :
            return EnumOperadora.AMEX;
        case  2 :
            return EnumOperadora.CIELO;
        case  3 :
            return EnumOperadora.CREDISHOP;
        case  4 :
            return EnumOperadora.FORTBRASIL;
        case  5 :
            return EnumOperadora.HIPERCARD;
        case  6 :
            return EnumOperadora.REDECARD;
        case  7 :
            return EnumOperadora.SANTANDER;
        case  8 :
            return EnumOperadora.GOODCARD;
        case  9 :
            return EnumOperadora.LIBERCARD;
        case 10 :
            return EnumOperadora.ELAVON;
        case 11 :
            return EnumOperadora.BANESECARD;
        case 12 :
            return EnumOperadora.TICKET;
        case 13 :
            return EnumOperadora.FIRSTDATA;
        case 14 :
            return EnumOperadora.AVISTA;
        case 15 :
            return EnumOperadora.TRIBANCO;
        case 16 :
            return EnumOperadora.CETELEM;
        case 17 :
            return EnumOperadora.SODEXO;
        case 18 :
            return EnumOperadora.VRBENEFICIOS;
        case 19 :
            return EnumOperadora.PROTEGECARD;
        case 20 :
            return EnumOperadora.VERO;
        case 21 :
            return EnumOperadora.LOSANGO;
        case 22 :
            return EnumOperadora.DACASA;
        case 23 :
            return EnumOperadora.POLICARD;
        case 24 :
            return EnumOperadora.VALECARD;
        case 25 :
            return EnumOperadora.COMPROCARD;
        case 26 :
            return EnumOperadora.FANCARD;
        case 27 :
            return EnumOperadora.ECXCARD;
        case 28 :
            return EnumOperadora.BANESCARD;
        case 29 :
            return EnumOperadora.CABAL;
        case 30 :
            return EnumOperadora.USECRED;
        case 32 :
            return EnumOperadora.PLANVALE;
        case 33 :
            return EnumOperadora.ABRAPETITE;
        case 34 :
            return EnumOperadora.SENF;
    }
};

EnumOperadora.prototype.list = function () {
    var a= [EnumOperadora.AMEX , EnumOperadora.CIELO, EnumOperadora.CREDISHOP, EnumOperadora.FORTBRASIL, EnumOperadora.HIPERCARD, EnumOperadora.REDECARD, EnumOperadora.SANTANDER, EnumOperadora.GOODCARD, EnumOperadora.LIBERCARD, EnumOperadora.ELAVON, EnumOperadora.BANESECARD, EnumOperadora.TICKET, EnumOperadora.FIRSTDATA, EnumOperadora.AVISTA, EnumOperadora.TRIBANCO, EnumOperadora.CETELEM, EnumOperadora.SODEXO, EnumOperadora.VRBENEFICIOS, EnumOperadora.PROTEGECARD, EnumOperadora.VERO, EnumOperadora.LOSANGO, EnumOperadora.DACASA, EnumOperadora.POLICARD, EnumOperadora.VALECARD, EnumOperadora.COMPROCARD, EnumOperadora.FANCARD, EnumOperadora.ECXCARD, EnumOperadora.BANESCARD, EnumOperadora.CABAL, EnumOperadora.USECRED, EnumOperadora.PLANVALE, EnumOperadora.ABRAPETITE, EnumOperadora.SENF];

    return a;
};