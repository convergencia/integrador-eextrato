/**
 * Created by Wanderson on 02/12/2016.
 */
function EnumHistorico(index, label) {
    this.index = index;
    this.label = label;
}

EnumHistorico.NONE = new EnumHistorico(0, '');
EnumHistorico.ALUGUEL_POS_INATIVO = new EnumHistorico(1, 'Aluguel POS inativo');
EnumHistorico.CANCELAMANTO_VENDA = new EnumHistorico(2, 'Cancelamento de venda');
EnumHistorico.ALUGUEL_POS_TAXA_CONECTIVIDADE = new EnumHistorico(3, 'Aluguel POS/Taxa Conectividade');
EnumHistorico.AJUSTE_CRED_DEB = new EnumHistorico(4, 'Ajuste Crédito/Débito');
EnumHistorico.CONSULTA_CHEQUES = new EnumHistorico(5, 'Consulta de Cheques');
EnumHistorico.CHARGEBACK = new EnumHistorico(6, 'Chargeback');
EnumHistorico.TARIFA = new EnumHistorico(7, 'Tarifa');


EnumHistorico.prototype.getById = function (id) {

    switch (id) {
        case  0 :
            return EnumHistorico.NONE;
        case  1 :
            return EnumHistorico.ALUGUEL_POS_INATIVO;
        case  2 :
            return EnumHistorico.CANCELAMANTO_VENDA;
        case  3 :
            return EnumHistorico.ALUGUEL_POS_TAXA_CONECTIVIDADE;
        case  4 :
            return EnumHistorico.AJUSTE_CRED_DEB;
        case  5 :
            return EnumHistorico.CONSULTA_CHEQUES;
        case  6 :
            return EnumHistorico.CHARGEBACK;
        case  7 :
            return EnumHistorico.TARIFA;
    }
};


EnumHistorico.prototype.list = function () {
    var a = [EnumHistorico.NONE, EnumHistorico.ALUGUEL_POS_INATIVO, EnumHistorico.CANCELAMANTO_VENDA ,EnumHistorico.ALUGUEL_POS_TAXA_CONECTIVIDADE, EnumHistorico.AJUSTE_CRED_DEB, EnumHistorico.CONSULTA_CHEQUES, EnumHistorico.CHARGEBACK, EnumHistorico.TARIFA];

    return a;
};