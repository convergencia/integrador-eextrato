function EnumBandeira(index, label) {
    this.index = index;
    this.label = label;
}
EnumBandeira.UNDEFINED = new EnumBandeira(00, 'Indefinido');
EnumBandeira.VISA = new EnumBandeira(01, 'Visa');
EnumBandeira.MASTERCARD = new EnumBandeira(02, 'MasterCard');
EnumBandeira.HIPERCARD = new EnumBandeira(03, 'HiperCard');
EnumBandeira.CREDISHOP = new EnumBandeira(04, 'CrediShop');
EnumBandeira.DINERSCLUB = new EnumBandeira(05, 'DinersClub');
EnumBandeira.FORTBRASIL = new EnumBandeira(06, 'FortBrasil');
EnumBandeira.ELO = new EnumBandeira(07, 'Elo');
EnumBandeira.CABAL = new EnumBandeira(08, 'Cabal');
EnumBandeira.AMEX = new EnumBandeira(09, 'Amex');
EnumBandeira.AGIPLAN = new EnumBandeira(10, 'Agiplan');
EnumBandeira.AURA = new EnumBandeira(11, 'Aura');
EnumBandeira.AVISTA = new EnumBandeira(12, 'Avista');
EnumBandeira.BANESCARD = new EnumBandeira(13, 'BanesCard');
EnumBandeira.CREDSYSTEM = new EnumBandeira(14, 'Credsystem');
EnumBandeira.CREDZ = new EnumBandeira(15, 'CredZ');
EnumBandeira.CUP = new EnumBandeira(16, 'CUP');
EnumBandeira.DISCOVER = new EnumBandeira(17, 'Discover');
EnumBandeira.ESPLANADA = new EnumBandeira(18, 'Esplanada');
EnumBandeira.GOODCARD = new EnumBandeira(19, 'GoodCard');
EnumBandeira.LIBERCARD = new EnumBandeira(20, 'LiberCard');
EnumBandeira.SICREDI = new EnumBandeira(21, 'Sicredi');
EnumBandeira.SODEXO = new EnumBandeira(22, 'Sodexo');
EnumBandeira.SOROCRED = new EnumBandeira(23, 'Sorocred');
EnumBandeira.TICKET_RESTAURANTE = new EnumBandeira(24, 'Ticket Restaurante');
EnumBandeira.TICKET_CAR = new EnumBandeira(25, 'Ticket Car');
EnumBandeira.TRICARD = new EnumBandeira(26, 'Tricard');
EnumBandeira.BANESCARD = new EnumBandeira(28, 'BanesCard');
EnumBandeira.VRBENEFICIOS = new EnumBandeira(29, 'VRBenefícios');
EnumBandeira.PROTEGECARD = new EnumBandeira(30, 'ProtegeCard');
EnumBandeira.BANRICARD = new EnumBandeira(33, 'BanriCard');
EnumBandeira.VERDECARD = new EnumBandeira(34, 'VerdeCard');
EnumBandeira.BANRICOMPRAS = new EnumBandeira(35, 'BanriCompras');
EnumBandeira.LOSANGO = new EnumBandeira(36, 'Losango');
EnumBandeira.DACASA = new EnumBandeira(37, 'DaCasa');
EnumBandeira.POLICARD = new EnumBandeira(38, 'Policard');
EnumBandeira.COMPROCARD = new EnumBandeira(39, 'Comprocard');
EnumBandeira.TICKET_ALIMENTACAO = new EnumBandeira(41, 'Ticket Alimentação');
EnumBandeira.FANCARD = new EnumBandeira(42, 'Fancard');
EnumBandeira.ECXCARD = new EnumBandeira(43, 'Ecxcard');
EnumBandeira.VALECARD = new EnumBandeira(44, 'Valecard');
EnumBandeira.USECRED = new EnumBandeira(45, 'Usecred');
EnumBandeira.PLANVALE = new EnumBandeira(46, 'Planvale');
EnumBandeira.SENFF = new EnumBandeira(47, 'Senff');
EnumBandeira.ABRAPETITE = new EnumBandeira(48, 'Abrapetite');


EnumBandeira.prototype.getById = function (id) {
    switch (id) {
    	case  0 :
    		return EnumBandeira.UNDEFINED;
        case  1 :
            return EnumBandeira.VISA;
        case  2 :
            return EnumBandeira.MASTERCARD;
        case  3 :
            return EnumBandeira.HIPERCARD;
        case  4 :
            return EnumBandeira.CREDISHOP;
        case  5 :
            return EnumBandeira.DINERSCLUB;
        case  6 :
            return EnumBandeira.FORTBRASIL;
        case  7 :
            return EnumBandeira.ELO;
        case  8 :
            return EnumBandeira.CABAL;
        case  9 :
            return EnumBandeira.AMEX;
        case 10 :
            return EnumBandeira.AGIPLAN;
        case 11 :
            return EnumBandeira.AURA;
        case 12 :
            return EnumBandeira.AVISTA;
        case 13 :
            return EnumBandeira.BANESCARD;
        case 14 :
            return EnumBandeira.CREDSYSTEM;
        case 15 :
            return EnumBandeira.CREDZ;
        case 16 :
            return EnumBandeira.CUP;
        case 17 :
            return EnumBandeira.DISCOVER;
        case 18 :
            return EnumBandeira.ESPLANADA;
        case 19 :
            return EnumBandeira.GOODCARD;
        case 20 :
            return EnumBandeira.LIBERCARD;
        case 21 :
            return EnumBandeira.SICREDI;
        case 22 :
            return EnumBandeira.SODEXO;
        case 23 :
            return EnumBandeira.SOROCRED;
        case 24 :
            return EnumBandeira.TICKET_RESTAURANTE;
        case 25 :
            return EnumBandeira.TICKET_CAR;
        case 26 :
            return EnumBandeira.TRICARD;
        case 28 :
            return EnumBandeira.BANESCARD;
        case 29 :
            return EnumBandeira.VRBENEFÍCIOS;
        case 30 :
            return EnumBandeira.PROTEGECARD;
        case 33 :
            return EnumBandeira.BANRICARD;
        case 34 :
            return EnumBandeira.VERDECARD;
        case 35 :
            return EnumBandeira.BANRICOMPRAS;
        case 36 :
            return EnumBandeira.LOSANGO;
        case 37 :
            return EnumBandeira.DACASA;
        case 38 :
            return EnumBandeira.POLICARD;
        case 39 :
            return EnumBandeira.COMPROCARD;
        case 41 :
            return EnumBandeira.TICKET_ALIMENTACAO;
        case 42 :
            return EnumBandeira.FANCARD;
        case 43 :
            return EnumBandeira.ECXCARD;
        case 44 :
            return EnumBandeira.VALECARD;
        case 45 :
            return EnumBandeira.USECRED;
        case 46 :
            return EnumBandeira.PLANVALE;
        case 47 :
            return EnumBandeira.SENFF;
        case 48 :
            return EnumBandeira.ABRAPETITE;

    }
};


EnumBandeira.prototype.list = function () {
    var a = [EnumBandeira.VISA, EnumBandeira.MASTERCARD, EnumBandeira.HIPERCARD, EnumBandeira.CREDISHOP, EnumBandeira.DINERSCLUB, EnumBandeira.FORTBRASIL, EnumBandeira.ELO, EnumBandeira.CABAL, EnumBandeira.AMEX, EnumBandeira.AGIPLAN, EnumBandeira.AURA, EnumBandeira.AVISTA, EnumBandeira.BANESCARD, EnumBandeira.CREDSYSTEM, EnumBandeira.CREDZ, EnumBandeira.CUP, EnumBandeira.DISCOVER, EnumBandeira.ESPLANADA, EnumBandeira.GOODCARD, EnumBandeira.LIBERCARD, EnumBandeira.SICREDI, EnumBandeira.SODEXO, EnumBandeira.SOROCRED, EnumBandeira.TICKET_RESTAURANTE, EnumBandeira.TICKET_CAR, EnumBandeira.TRICARD, EnumBandeira.BANESCARD, EnumBandeira.VRBENEFICIOS, EnumBandeira.PROTEGECARD, EnumBandeira.BANRICARD, EnumBandeira.VERDECARD, EnumBandeira.BANRICOMPRAS, EnumBandeira.LOSANGO, EnumBandeira.DACASA, EnumBandeira.POLICARD, EnumBandeira.COMPROCARD, EnumBandeira.TICKET_ALIMENTACAO, EnumBandeira.FANCARD, EnumBandeira.ECXCARD, EnumBandeira.VALECARD, EnumBandeira.USECRED, EnumBandeira.PLANVALE, EnumBandeira.SENFF, EnumBandeira.ABRAPETITE];

    return a;
};