/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.web.rest;

import io.convergencia.eextrato.model.RetornoAjusteTarifa;
import io.convergencia.eextrato.params.ParamSearch;
import io.convergencia.eextrato.repository.RetornoAjusteTarifaDao;
import io.convergencia.eextrato.specifications.AjusteTarifaSpecification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/retorno-ajuste-tarifa")
public class RetornoAjusteTarifaController {

    @Autowired
    private RetornoAjusteTarifaDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<RetornoAjusteTarifa> getAll() {
        List<RetornoAjusteTarifa> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RetornoAjusteTarifa get(@PathVariable Long id) {
        return dao.findOne(id);
    }
    
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<Object> get() {
        Map<String, Long> r = new HashMap<>();
        r.put("count", dao.count());
        return ResponseEntity.ok(r);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public List<RetornoAjusteTarifa> search(@RequestBody ParamSearch ps) {
        Specification<RetornoAjusteTarifa> searchPagamento = AjusteTarifaSpecification.searchPagamento(ps);

        List<RetornoAjusteTarifa> target = new ArrayList<>();
        dao.findAll(searchPagamento).forEach(target::add);
        return target;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody RetornoAjusteTarifa obj) {
        dao.save(obj);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}
