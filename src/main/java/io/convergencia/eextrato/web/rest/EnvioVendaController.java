/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.web.rest;

import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.params.ParamSearch;
import io.convergencia.eextrato.repository.EnvioVendaDao;
import io.convergencia.eextrato.specifications.VendaSpecification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/envio-venda")
public class EnvioVendaController {

    @Autowired
    private EnvioVendaDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<EnvioVenda> getAll() {
        List<EnvioVenda> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public EnvioVenda get(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public EEnvioEvento[] getStatus() {
        return EEnvioEvento.values();
    }

    @RequestMapping(value = "/count/pendentes", method = RequestMethod.GET)
    public ResponseEntity<Object> countPendentes() {
        Map<String, Long> r = new HashMap<>();
        r.put("count", dao.countByStatus(EEnvioEvento.PENDENTE));
        return ResponseEntity.ok(r);
    }

    @RequestMapping(value = "/count/enviados", method = RequestMethod.GET)
    public ResponseEntity<Object> countEnviados() {
        Map<String, Long> r = new HashMap<>();
        r.put("count", dao.countByStatus(EEnvioEvento.AGUARDANDO_RETORNO));
        return ResponseEntity.ok(r);
    }

    @RequestMapping(value = "/count/erros", method = RequestMethod.GET)
    public ResponseEntity<Object> countErros() {
        Map<String, Long> r = new HashMap<>();
        r.put("count", dao.countByStatus(EEnvioEvento.ERRO_ENVIO));
        return ResponseEntity.ok(r);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public List<EnvioVenda> search(@RequestBody ParamSearch ps) {
        Specification<EnvioVenda> searchPagamento = VendaSpecification.searchEnvio(ps);

        List<EnvioVenda> target = new ArrayList<>();
        dao.findAll(searchPagamento).forEach(target::add);
        return target;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody EnvioVenda obj) {
        dao.save(obj);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}
