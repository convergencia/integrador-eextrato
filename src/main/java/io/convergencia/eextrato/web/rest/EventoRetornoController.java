/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.web.rest;

import io.convergencia.eextrato.model.EventoRetorno;
import io.convergencia.eextrato.repository.EventoRetornoDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/evento-retorno")
public class EventoRetornoController {
    @Autowired
    private EventoRetornoDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<EventoRetorno> getAll() {
        List<EventoRetorno> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public EventoRetorno get(@PathVariable Long id) {
        return dao.findOne(id);
    }
    
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<Object> get() {
        Map<String, Long> r = new HashMap<>();
        r.put("count", dao.count());
        return ResponseEntity.ok(r);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody EventoRetorno obj) {
        dao.save(obj);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}
