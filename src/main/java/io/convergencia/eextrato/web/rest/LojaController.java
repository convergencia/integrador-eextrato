/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.web.rest;

import io.convergencia.eextrato.model.LojaERP;
import io.convergencia.eextrato.repository.LojaERPDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/loja")
public class LojaController {
    @Autowired
    private LojaERPDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<LojaERP> getAll() {
        List<LojaERP> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public LojaERP get(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public LojaERP save(@RequestBody LojaERP obj) {
        dao.save(obj);
        return obj;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}
