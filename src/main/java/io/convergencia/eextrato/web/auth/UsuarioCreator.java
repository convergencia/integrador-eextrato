/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.web.auth;

import io.convergencia.eextrato.api.enuns.EPerfil;
import io.convergencia.eextrato.model.Usuario;
import io.convergencia.eextrato.repository.UsuarioDao;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
public class UsuarioCreator {

    @Autowired
    private UsuarioDao ud;

    @PostConstruct
    public void init() {

        Usuario u = ud.findOneByUsername("admin");
        if (null == u) {
            u = new Usuario();
            u.setEmail("");
            u.setNome("Administrador");
            u.setPerfil(EPerfil.ADMIN);
            u.setUsername("admin");
            u.setPassword("123456");

            ud.save(u);
        }
    }

}
