/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.api;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MoneyConvert extends XmlAdapter<String, BigDecimal> {

    @Override
    public BigDecimal unmarshal(String v) throws Exception {
        return new BigDecimal(v).setScale(2, RoundingMode.HALF_EVEN);
    }

    @Override
    public String marshal(BigDecimal v) throws Exception {
        return v.setScale(2, RoundingMode.HALF_EVEN).toString();
    }

}
