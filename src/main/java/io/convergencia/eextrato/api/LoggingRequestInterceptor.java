/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.api;

import java.io.IOException;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.FileCopyUtils;

/**
 *
 * @author Wanderson
 */
public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
        System.out.println("===========================request begin================================================");
        System.out.println(String.format("URI         : %s", request.getURI()));
        System.out.println(String.format("Method      : %s", request.getMethod()));
        System.out.println(String.format("Headers     : %s", request.getHeaders()));
        System.out.println(String.format("Request body: %s", new String(body, "UTF-8")));
        System.out.println("==========================request end================================================");
    }

    private void traceResponse(ClientHttpResponse response) throws IOException {
        final byte[] copyToByteArray = FileCopyUtils.copyToByteArray(response.getBody());
        String line = new String(copyToByteArray);

        System.out.println("============================response begin==========================================");
        System.out.println(String.format("Status code  : %s", response.getStatusCode()));
        System.out.println(String.format("Status text  : %s", response.getStatusText()));
        System.out.println(String.format("Headers      : %s", response.getHeaders()));
        System.out.println(String.format("Response body: %s", line));
        System.out.println("=======================response end=================================================");
    }
}
