/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.api;

import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.model.LoteEnvio;
import java.util.Collection;

/**
 *
 * @author wanderson
 */
public interface IProcessaVenda {
    void doGerarLote(Collection<EnvioVenda> evs);
    void doCapturarRetorno(Collection<LoteEnvio> les);
}
