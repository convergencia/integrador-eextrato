/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.api.enuns;

/**
 *
 * @author wanderson
 */
public enum EStatusLote {
    CRIADO,
    ENVIADO,
    ENVIADO_PARCIAL,
    CONFIRMADO,
    CONFIRMADO_PARCIAL,
    ERRO_ENVIO
}
