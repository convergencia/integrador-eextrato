/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.api;

import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import convergencia.api.Log;
import io.convergencia.eextrato.model.Erro;
import io.convergencia.eextrato.model.Filtro;
import io.convergencia.eextrato.model.RemessaResponse;
import io.convergencia.eextrato.model.RetornoResponse;
import io.convergencia.eextrato.model.VendasERP;
import io.convergencia.eextrato.util.Util;

/**
 *
 * @author Wanderson
 */
public class ApiEnvio {

	private final RestTemplate rest = new RestTemplate(new HttpComponentsAsyncClientHttpRequestFactory());
	private final HttpHeaders requestHeaders;

	static {

		TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s, Socket socket) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s, Socket socket) {
			}

			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s, SSLEngine sslEngine) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			SSLContext.setDefault(sc);
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception ex) {
			Logger.getLogger(ApiEnvio.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public ApiEnvio(String chaveAcesso, String chaveSecreta) {
		this.requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_XML);
		requestHeaders.set("chaveAcesso", chaveAcesso);
		requestHeaders.set("chaveSecreta", chaveSecreta);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
		rest.setMessageConverters(messageConverters);

		// rest.getInterceptors().add(new LoggingRequestInterceptor());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
   private RetornoResponse recuperar(Filtro f, String url) {
		try {
			Util.removeTime(f.getDataPagamentoFim());
			Util.removeTime(f.getDataPagamentoInicio());
			Util.removeTime(f.getDataVendaFim());
			Util.removeTime(f.getDataVendaInicio());

			HttpEntity<?> requestEntity = new HttpEntity(f, requestHeaders);
			ResponseEntity<RetornoResponse> respo = rest.exchange(new URI(url), HttpMethod.POST, requestEntity,
					RetornoResponse.class);

			return respo.getBody();
		} catch (URISyntaxException | RuntimeException ex) {
			RetornoResponse resp = new RetornoResponse();
			resp.setErro(new Erro());
			resp.getErro().setDescricao(ex.getMessage());

			if (ex instanceof RestClientException) {
			   Log.error(((RestClientException) ex).getMostSpecificCause());
			}

			return resp;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
   public RemessaResponse enviarVenda(VendasERP vendas, String url) {
		try {

			vendas.getVendas().forEach((t) -> {

				t.setAutorizacao(null);

				if (t.getCodigoLoja() != null) {
					t.setCodigoEstabelecimento(null);
				}

				if (t.getNsu() == null || "0".equals(t.getNsu())) {
					t.setNsu("00000000");
				}

			});

			HttpEntity<?> requestEntity = new HttpEntity(vendas, requestHeaders);
			ResponseEntity<RemessaResponse> respo = rest.exchange(new URI(url), HttpMethod.POST, requestEntity,
					RemessaResponse.class);
			return respo.getBody();

		} catch (URISyntaxException | RuntimeException ex) {
			RemessaResponse resp = new RemessaResponse();
			resp.setErro(new Erro());
			resp.getErro().setDescricao(ex.getMessage());
			return resp;
		}

	}

	public RetornoResponse recuperarPagamento(Filtro f, String url) {
		if (f != null) {
			f.setTipoArquivo(1);
			return recuperar(f, url);
		}
		return null;
	}

	public RetornoResponse recuperarVenda(Filtro f, String url) {
		if (f != null) {
			f.setTipoArquivo(2);
			return recuperar(f, url);
		}
		return null;
	}

	public RetornoResponse recuperarAntecipacao(Filtro f, String url) {
		if (f != null) {
			f.setTipoArquivo(3);
			return recuperar(f, url);
		}
		return null;
	}

	public RetornoResponse recuperarAjusteTariva(Filtro f, String url) {
		if (f != null) {
			f.setTipoArquivo(4);
			return recuperar(f, url);
		}
		return null;
	}
}
