/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.ApiEnvio;
import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.api.enuns.EStatusLote;
import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.model.ErroSistema;
import io.convergencia.eextrato.model.LoteEnvio;
import io.convergencia.eextrato.model.RemessaResponse;
import io.convergencia.eextrato.model.VendaERPErro;
import io.convergencia.eextrato.model.VendasERP;
import io.convergencia.eextrato.repository.EnvioVendaDao;
import io.convergencia.eextrato.repository.ErroSistemaDao;
import io.convergencia.eextrato.repository.LoteEnvioDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EnvioVendaService {

    @Autowired
    private Environment env;

    @Autowired
    private EnvioVendaDao vendaDao;

    @Autowired
    private ErroSistemaDao erroSistemaDao;

    @Autowired
    private LoteEnvioDao loteEnvioDao;

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void doEnvioVenda(VendasERP vendas, LoteEnvio loteEnvio) {

        ApiEnvio apiEnvio = new ApiEnvio(env.getProperty("eextrato.chaveAcesso"), env.getProperty("eextrato.chaveSecreta"));
        RemessaResponse response = apiEnvio.enviarVenda(vendas, env.getProperty("eextrato.urlRemessa"));

        if (response.getStatus() == null) {
            if (response.getErro() != null) {
                vendas.getVendas().forEach((t) -> {
                    doGravarErro(t, response.getErro().toString());
                });
                loteEnvio.setStatusVenda(EStatusLote.ERRO_ENVIO);
                loteEnvio.getVendas().forEach((loteEnvioVenda) -> {
                    loteEnvioVenda.setStatusVenda(EStatusLote.ERRO_ENVIO);
                });
            } else {
                loteEnvio.setStatusVenda(EStatusLote.ENVIADO_PARCIAL);
                vendas.getVendas().forEach((t) -> {
                    VendaERPErro vendaErro = new VendaERPErro(t.getId());
                    if (response.getVendaERPErro().contains(vendaErro)) {
                        List<VendaERPErro> l = (List<VendaERPErro>) response.getVendaERPErro();

                        VendaERPErro erro = l.get(l.indexOf(vendaErro));
                        doGravarErro(t, erro.getDescricao());

                        loteEnvio.getVendaByEnvio(t).setStatusVenda(EStatusLote.ERRO_ENVIO);

                    } else {
                        t.setStatus(EEnvioEvento.AGUARDANDO_RETORNO);
                        vendaDao.save(t);

                        loteEnvio.getVendaByEnvio(t).setStatusVenda(EStatusLote.ENVIADO);
                    }
                });

            }
        } else {
            vendas.getVendas().forEach((t) -> {
                t.setStatus(EEnvioEvento.AGUARDANDO_RETORNO);
                vendaDao.save(t);

                loteEnvio.setStatusVenda(EStatusLote.ENVIADO);
                loteEnvio.getVendas().forEach((loteEnvioVenda) -> {
                    loteEnvioVenda.setStatusVenda(EStatusLote.ENVIADO);
                });

            });
        }
        loteEnvioDao.save(loteEnvio);
    }

    private void doGravarErro(EnvioVenda t, String msg) {
        t.setStatus(EEnvioEvento.ERRO_ENVIO);
        vendaDao.save(t);

        ErroSistema erro = new ErroSistema();
        erro.setOperacao("ENVIO_VENDA");
        erro.setIdOperacao(t.getId());
        erro.setMensagem(msg);
        erroSistemaDao.save(erro);
    }

}
