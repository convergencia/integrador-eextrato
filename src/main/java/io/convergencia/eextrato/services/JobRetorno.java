/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.IProcessaVenda;
import io.convergencia.eextrato.api.enuns.EStatusLote;
import io.convergencia.eextrato.model.Filtro;
import io.convergencia.eextrato.model.LoteEnvio;
import io.convergencia.eextrato.repository.LoteEnvioDao;
import io.convergencia.eextrato.util.Util;
import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnExpression("${eextrato.jobRetorno.enabled}")
public class JobRetorno {

    @Autowired
    private Environment env;

    @Autowired
    private RetornoPagamentoService pagamentoService;

    @Resource(name = "${eextrato.venda.tipoProcesso}")
    private IProcessaVenda processaVenda;

    @Autowired
    private RetornoAntecipacaoService antecipacaoService;

    @Autowired
    private RetornoAjusteTarifasService ajusteTarifasService;

    @Autowired
    private LoteEnvioDao loteEnvioDao;

    @Scheduled(fixedDelayString = "${eextrato.schedule.delayRetornoVenda}", initialDelay = 5000L)
    public void servicoRetornoPagamento() {

        Calendar c = Calendar.getInstance();
        Calendar dtProc = Calendar.getInstance();

        System.out.println("Inciando retorno de pagametno " + c.getTime().toLocaleString());

        c.add(Calendar.DAY_OF_YEAR, -1 * env.getProperty("eextrato.schedule.processamentoEmD", Integer.class, 1));
        Util.removeTime(c);
        Util.removeTime(dtProc);

        Filtro f = new Filtro();
        f.setDataPagamentoInicio(c.getTime());
        f.setDataPagamentoFim(c.getTime());
        f.setPagina(1);
        f.setTipoArquivo(1);

        pagamentoService.doRetorno(f, dtProc.getTime());
        System.out.println("Terminou retorno de pagametno " + Calendar.getInstance().getTime().toLocaleString());

    }

    @Scheduled(fixedDelayString = "${eextrato.schedule.delayRetornoVenda}", initialDelay = 5000L)
    public void servicoRetornoVenda() {

        Calendar c = Calendar.getInstance();
        System.out.println("Inciando retorno de venda " + c.getTime().toLocaleString());

        List<LoteEnvio> loteEnvios = loteEnvioDao.findTop100ByStatusVendaInOrderByDataAsc(EStatusLote.ENVIADO, EStatusLote.CONFIRMADO_PARCIAL, EStatusLote.ENVIADO_PARCIAL);

        processaVenda.doCapturarRetorno(loteEnvios);

        System.out.println("Terminou retorno de venda " + Calendar.getInstance().getTime().toLocaleString());

    }

    @Scheduled(cron = "${eextrato.schedule.cronTimeRetornoAntecipacao}")
    public void servicoRetornoAntecipacao() {

        Calendar c = Calendar.getInstance();
        System.out.println("Inciando retorno de antecipacao " + c.getTime().toLocaleString());
        c.add(Calendar.DAY_OF_YEAR, env.getProperty("eextrato.schedule.processamentoEmD", Integer.class, -1));
        Util.removeTime(c);

        Filtro f = new Filtro();
        f.setDataPagamentoInicio(c.getTime());
        f.setDataPagamentoFim(c.getTime());
        f.setPagina(1);
        f.setTipoArquivo(3);

        antecipacaoService.doRetorno(f, c.getTime());
        System.out.println("Terminou retorno de antecipacao " + Calendar.getInstance().getTime().toLocaleString());

    }

    @Scheduled(cron = "${eextrato.schedule.cronTimeRetornoAjusteTarifa}")
    public void servicoRetornoAjusteTarifa() {

        Calendar c = Calendar.getInstance();
        System.out.println("Inciando retorno de ajuste e tarifa " + c.getTime().toLocaleString());
        c.add(Calendar.DAY_OF_YEAR, env.getProperty("eextrato.schedule.processamentoEmD", Integer.class, -1));
        Util.removeTime(c);

        Filtro f = new Filtro();
        f.setDataPagamentoInicio(c.getTime());
        f.setDataPagamentoFim(c.getTime());
        f.setPagina(1);
        f.setTipoArquivo(4);

        ajusteTarifasService.doRetorno(f, c.getTime());
        System.out.println("Terminou retorno de ajuste e tarifa " + Calendar.getInstance().getTime().toLocaleString());

    }

}
