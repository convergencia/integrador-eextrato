/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import convergencia.api.Log;
import io.convergencia.eextrato.api.IProcessaVenda;
import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.model.ErroSistema;
import io.convergencia.eextrato.repository.EnvioVendaDao;
import io.convergencia.eextrato.repository.ErroSistemaDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import org.restexpress.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
@ConditionalOnExpression("${eextrato.jobCaptura.enabled}")
public class JobCaptura {

    @Autowired
    private EnvioVendaDao envioVendaDao;

    @Autowired
    private ErroSistemaDao erroSistemaDao;

    @Resource(name = "${eextrato.venda.tipoProcesso}")
    private IProcessaVenda processaVenda;

    @Scheduled(fixedDelayString = "${eextrato.schedule.delayEnvioVendaLote}", initialDelay = 5000L)
    public void servicoEnvio() {

        try {
            envioVendaDao.addPendencia();
        } catch (Exception ex) {
            //mostra log de erro procedure integracao ERP
            Log.error(ex);
        }

        List<EnvioVenda> evs = envioVendaDao.findTop100ByStatus(EEnvioEvento.PENDENTE);

        if (evs == null || evs.isEmpty()) {
            return;
        }

        boolean encontrouErro = false;
        Iterator<EnvioVenda> iter = evs.iterator();
        while (iter.hasNext()) {
            List<String> camposInvalidos = new ArrayList<>();

            EnvioVenda envioVenda = iter.next();
            Date dataVenda = envioVenda.getDataVenda();
            String nsu = envioVenda.getNsu();
            String autorizacao = envioVenda.getAutorizacao();
            Integer plano = envioVenda.getPlano();
            BigDecimal valor = envioVenda.getValor();
            Integer codLoja = envioVenda.getCodigoLoja();
            String codEstab = envioVenda.getCodigoEstabelecimento();
            String produto = envioVenda.getProduto();

            if (dataVenda == null) {
                camposInvalidos.add("dataVenda");
            }

            if ((nsu == null || nsu.isEmpty() || "0".equals(nsu)) && (autorizacao == null || autorizacao.isEmpty() || "0".equals(autorizacao))) {
                camposInvalidos.add("nsu|autorizacao");
            }

            if (plano == null || "C".equals(produto) && Integer.valueOf(0).equals(plano)) {
                camposInvalidos.add("plano");
            }

            if ((codLoja == null || codLoja == 0) && (codEstab == null || codEstab.isEmpty())) {
                camposInvalidos.add("codLoja|codEstab");
            }

            if (produto == null || !("C".equals(produto) || "D".equals(produto))) {
                camposInvalidos.add("produto");
            }

            if (valor == null || BigDecimal.ZERO.compareTo(valor) >= 0) {
                camposInvalidos.add("valor");
            }

            if (!camposInvalidos.isEmpty()) {

                iter.remove();

                encontrouErro = true;
                String msg = "campos " + StringUtils.join(",", camposInvalidos) + " invalidos";
                ErroSistema erroSistema = new ErroSistema();
                erroSistema.setOperacao("ENVIO_VENDA");
                erroSistema.setIdOperacao(envioVenda.getId());
                erroSistema.setMensagem(msg);

                erroSistemaDao.save(erroSistema);

                envioVenda.setStatus(EEnvioEvento.ERRO_ENVIO);
                envioVendaDao.save(envioVenda);

            }

        }

        if (!encontrouErro && !evs.isEmpty()) {
            processaVenda.doGerarLote(evs);
        }

    }

}
