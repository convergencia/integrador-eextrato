/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.enuns.ERetornoEvento;
import io.convergencia.eextrato.api.ApiEnvio;
import io.convergencia.eextrato.model.EventoRetorno;
import io.convergencia.eextrato.model.Filtro;
import io.convergencia.eextrato.model.RetornoResponse;
import io.convergencia.eextrato.repository.EventoRetornoDao;
import io.convergencia.eextrato.repository.RetornoAjusteTarifaDao;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:/application.properties")
public class RetornoAjusteTarifasService {

    @Autowired
    private Environment env;

    @Autowired
    private RetornoAjusteTarifaDao ajusteTarifaDao;

    @Autowired
    private EventoRetornoDao eventoRetornoDao;

    public void doRetorno(Filtro f, Date dataProcessamento) {
        EventoRetorno er = Optional.ofNullable(eventoRetornoDao.findByProcessarDataAndTipoArquivo(dataProcessamento, f.getTipoArquivo())).orElse(new EventoRetorno());

        if (!er.getStatus().equals(ERetornoEvento.FECHADO)) {

            if (er.getStatus().equals(ERetornoEvento.ABERTO)) {
                er.setTipoArquivo(f.getTipoArquivo());
                er.setProcessarData(dataProcessamento);
                er.setInicio(Calendar.getInstance().getTime());
            } else {
                f.setPagina(er.getPaginaAtual() + 1);
            }

            eventoRetornoDao.save(er);

            ApiEnvio apiEnvio = new ApiEnvio(env.getProperty("eextrato.chaveAcesso"), env.getProperty("eextrato.chaveSecreta"));
            RetornoResponse rr;
            do {
                rr = doRequestServer(apiEnvio, f);

                if (rr.getPaginaAtual() != null) {
                    er.setStatus(ERetornoEvento.ANDAMENTO);
                    er.setTotalPaginas(rr.getTotalPaginas());
                    er.setPaginaAtual(rr.getPaginaAtual());
                    er.setUltimaExecucao(Calendar.getInstance().getTime());
                    eventoRetornoDao.save(er);
                }

                System.out.println(String.format("Ajuste tarifa - Pagina %s de %s", rr.getPaginaAtual(), rr.getTotalPaginas()));
            } while (rr.getPaginaAtual() != null && rr.getPaginaAtual() < rr.getTotalPaginas());

            er.setStatus(ERetornoEvento.FECHADO);
            er.setFim(Calendar.getInstance().getTime());
            eventoRetornoDao.save(er);

            try {
                ajusteTarifaDao.processaRetornoErp();
            } catch (Exception e) {
                // ignored for not break proccess
            }
        }
    }

    private RetornoResponse doRequestServer(ApiEnvio apiEnvio, Filtro f) {
        RetornoResponse rr = apiEnvio.recuperarAjusteTariva(f, env.getProperty("eextrato.urlRetorno"));
        if (rr.getPaginaAtual() != null) {
            f.setPagina(rr.getPaginaAtual() + 1);

            if (!rr.getAjustes().isEmpty()) {
                rr.getAjustes().removeIf((t) -> {
                    return null == t.getCodigoEstabelecimento(); //To change body of generated lambdas, choose Tools | Templates.
                });
                ajusteTarifaDao.save(rr.getAjustes());
            }
        }

        return rr;
    }
}
