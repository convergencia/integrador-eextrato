/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.enuns.EStatusLote;
import io.convergencia.eextrato.model.LoteEnvio;
import io.convergencia.eextrato.model.LoteEnvioVenda;
import io.convergencia.eextrato.model.VendasERP;
import io.convergencia.eextrato.repository.LoteEnvioDao;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnExpression("${eextrato.jobEnvio.enabled}")
public class JobEnvio {

    @Autowired
    private LoteEnvioDao loteEnvioDao;

    @Autowired
    private EnvioVendaService vendaService;

    @Scheduled(fixedDelayString = "${eextrato.schedule.delayEnvioVenda}", initialDelay = 5000L)
    public void servicoEnvio() {

        List<LoteEnvio> loteEnvios = loteEnvioDao.findTop100ByStatusVendaInOrderByDataAsc(EStatusLote.CRIADO, EStatusLote.ENVIADO_PARCIAL, EStatusLote.ERRO_ENVIO);

        loteEnvios
                .parallelStream()
                .forEach((loteEnvio) -> {
                    Collection<LoteEnvioVenda> vendas = loteEnvio.getVendas();
                    if (vendas.isEmpty()) {
                        loteEnvioDao.delete(loteEnvio);
                    } else {
                        VendasERP vendasERP = new VendasERP();
                        vendas.forEach((loteEnvioVenda) -> {
                            EStatusLote statusEnvio = loteEnvioVenda.getStatusVenda();
                            if (!(statusEnvio == EStatusLote.ENVIADO || statusEnvio == EStatusLote.CONFIRMADO)) {
                                vendasERP.add(loteEnvioVenda.getVenda());
                            }
                        });
                        if (vendasERP.getVendas().isEmpty()) {
                            loteEnvio.setStatusVenda(EStatusLote.ENVIADO);
                            loteEnvioDao.save(loteEnvio);
                        } else {
                            vendaService.doEnvioVenda(vendasERP, loteEnvio);
                        }
                    }
                });
    }
}
