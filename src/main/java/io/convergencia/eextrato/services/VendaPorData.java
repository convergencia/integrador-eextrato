/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.IProcessaVenda;
import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.model.Filtro;
import io.convergencia.eextrato.model.LoteEnvio;
import io.convergencia.eextrato.model.LoteEnvioVenda;
import io.convergencia.eextrato.repository.EnvioVendaDao;
import io.convergencia.eextrato.repository.LoteEnvioDao;
import io.convergencia.eextrato.util.Util;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;

/**
 *
 * @author wanderson
 */
@Component(value = "VendaPorData")
public class VendaPorData implements IProcessaVenda {

    @Autowired
    private EnvioVendaDao envioVendaDao;

    @Autowired
    private LoteEnvioDao loteEnvioDao;

    @Autowired
    private RetornoVendaService vendaService;

    @Override
    public void doGerarLote(Collection<EnvioVenda> evs) {
        Iterator<EnvioVenda> it = evs.iterator();
        while (it.hasNext()) {
            LoteEnvio le = new LoteEnvio();
            do {
                EnvioVenda ev = it.next();
                le.getVendas().add(new LoteEnvioVenda(le, ev));

            } while (le.getVendas().size() < 100 && it.hasNext());

            doPersist(le);

        }
    }

    @Override
    public void doCapturarRetorno(Collection<LoteEnvio> les) {
        les.forEach((loteEnvio) -> {
            System.out.println("Buscando lote " + loteEnvio.getId());

            Calendar ca = Calendar.getInstance();
            ca.setTimeInMillis(loteEnvio.getData().getTime());
            Util.removeTime(ca);

            loteEnvio.getVendas().forEach((lev) -> {
                Filtro f = new Filtro();
                f.setDadosCliente(loteEnvio.getId());
                f.setDataVendaInicio(ca.getTime());
                f.setDataVendaFim(ca.getTime());
                f.setPagina(1);
                f.setTipoArquivo(2);
                f.setTipoVenda(1);
                f.setDadosCliente(lev.getVenda().getAreaCliente());

                vendaService.doRetorno(f, loteEnvio);
            });

        });
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    private void doPersist(LoteEnvio le) {
        loteEnvioDao.save(le);
        le.getVendas().forEach((t) -> {
            t.getVenda().setAreaCliente(UUID.randomUUID().toString().toUpperCase());
            t.getVenda().setStatus(EEnvioEvento.ANDAMENTO);
            envioVendaDao.save(t.getVenda());
        });
    }

}
