/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.services;

import io.convergencia.eextrato.api.ApiEnvio;
import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.api.enuns.EStatusLote;
import io.convergencia.eextrato.model.EnvioVenda;
import io.convergencia.eextrato.model.Filtro;
import io.convergencia.eextrato.model.LoteEnvio;
import io.convergencia.eextrato.model.LoteEnvioVenda;
import io.convergencia.eextrato.model.RetornoResponse;
import io.convergencia.eextrato.model.RetornoVenda;
import io.convergencia.eextrato.repository.EnvioVendaDao;
import io.convergencia.eextrato.repository.LoteEnvioDao;
import io.convergencia.eextrato.repository.LoteEnvioVendaDao;
import io.convergencia.eextrato.repository.RetornoVendaDao;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RetornoVendaService {

    @Autowired
    private Environment env;

    @Autowired
    private RetornoVendaDao retornoVendaDao;

    @Autowired
    private EnvioVendaDao envioVendaDao;

    @Autowired
    private LoteEnvioDao loteEnvioDao;

    @Autowired
    private LoteEnvioVendaDao loteEnvioVendaDao;

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void doRetorno(Filtro f, LoteEnvio loteEnvio) {

        ApiEnvio apiEnvio = new ApiEnvio(env.getProperty("eextrato.chaveAcesso"), env.getProperty("eextrato.chaveSecreta"));
        RetornoResponse rr = apiEnvio.recuperarVenda(f, env.getProperty("eextrato.urlRetorno"));

        if (!rr.getVendas().isEmpty()) {

            if (loteEnvio.getVendas().size() == rr.getVendas().size()) {
                loteEnvio.setStatusVenda(EStatusLote.CONFIRMADO);
            } else {
                loteEnvio.setStatusVenda(EStatusLote.CONFIRMADO_PARCIAL);
            }

            loteEnvioDao.save(loteEnvio);

            rr.getVendas().forEach((RetornoVenda retornoVenda) -> {
                String areaCliente = retornoVenda.getAreaCliente();
                LoteEnvioVenda loteEnvioVenda = loteEnvio.getVendaByAreaCliente(areaCliente);
                if (loteEnvioVenda != null) {

                    EnvioVenda envioVenda = loteEnvioVenda.getVenda();

                    envioVenda.setStatus(EEnvioEvento.CONCILIADO);
                    envioVendaDao.save(envioVenda);

                    if (loteEnvioVenda.getStatusVenda() != EStatusLote.CONFIRMADO) {

                        loteEnvioVenda.setStatusVenda(EStatusLote.CONFIRMADO);
                        loteEnvioVendaDao.save(loteEnvioVenda);

                        retornoVenda.setChaveErp(envioVenda.getChaveErp());
                        retornoVenda.setDtHrRetorno(new Date());
                        retornoVendaDao.save(retornoVenda);
                    }

                }

            });

            retornoVendaDao.processaRetornoErp();
        }

    }

}
