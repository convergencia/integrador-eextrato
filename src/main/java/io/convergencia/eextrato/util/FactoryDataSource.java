/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.util;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
public class FactoryDataSource {

    @Autowired
    private Environment env;

    public DataSource getDataSource() {
        String driver = env.getProperty("db.default.driver", "");

        DriverManagerDataSource dataSource;
        String url;
        switch (driver) {
            case "mssql":

                if (!env.getProperty("db.default.instance", "").isEmpty()) {
                    url = String.format("jdbc:sqlserver://%s\\%s;databaseName=%s",
                            env.getProperty("db.default.host", ""),
                            env.getProperty("db.default.instance", ""),
                            env.getProperty("db.default.base", ""));
                } else {
                    url = String.format("jdbc:sqlserver://%s:%s;databaseName=%s",
                            env.getProperty("db.default.host", ""),
                            env.getProperty("db.default.port", ""),
                            env.getProperty("db.default.base", ""));
                }

                dataSource = new DriverManagerDataSource();
                dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                dataSource.setUrl(url);
                dataSource.setUsername(env.getProperty("db.default.user", ""));
                dataSource.setPassword(env.getProperty("db.default.pass", ""));
                return dataSource;

            case "postgre":

                url = String.format("jdbc:postgresql://%s:%s/%s",
                        env.getProperty("db.default.host", ""),
                        env.getProperty("db.default.port", ""),
                        env.getProperty("db.default.base", ""));

                dataSource = new DriverManagerDataSource();
                dataSource.setDriverClassName("org.postgresql.Driver");
                dataSource.setUrl(url);
                dataSource.setUsername(env.getProperty("db.default.user", ""));
                dataSource.setPassword(env.getProperty("db.default.pass", ""));
                return dataSource;

            default:
                throw new RuntimeException("Driver not selected");
        }
    }

    public String getDialect() {
        String driver = env.getProperty("db.default.driver", "");

        switch (driver) {
            case "mssql":
                return "org.hibernate.dialect.SQLServer2008Dialect";
            case "postgre":
                return "org.hibernate.dialect.PostgreSQLDialect";
            default:
                throw new RuntimeException("Driver not selected");
        }

    }
}
