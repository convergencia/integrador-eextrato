package io.convergencia.eextrato.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.DynamicUpdate;

@Entity(name = "INTEGRA_CONC_TEF_RET_VENDA")
@DynamicUpdate
@XmlRootElement(name = "venda")
public class RetornoVenda extends Entidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "AGENCIA")
    private Integer agencia;

    @Column(name = "AUTORIZACAO")
    private String autorizacao;

    @Column(name = "BANCO")
    private Integer banco;

    @Column(name = "BANDEIRA")
    private Integer bandeira;

    @Column(name = "CODIGO_ESTABELECIMENTO")
    private Long codigoEstabelecimento;

    @Column(name = "CODIGO_LOJA_ERP")
    private Integer codigoLojaErp;

    @Column(name = "CONTA")
    private Integer conta;

    @Column(name = "DATA_VENDA")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataVenda;

    @Column(name = "NSU")
    private Long nsu;

    @Column(name = "NUMERO_LOTE")
    private Integer numeroLote;

    @Column(name = "PLANO")
    private Integer plano;

    @Column(name = "PRODUTO")
    private Character produto;

    @Column(name = "REDE")
    private Integer rede;

    @Column(name = "STATUS_CONCILIACAO")
    private Integer statusConciliacao;

    @Column(name = "DATA_CREDITO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCredito;

    @Column(name = "NUMERO_CARTAO")
    private String numeroCartao;

    @Column(name = "TAXA_ADMINISTRACAO")
    private BigDecimal taxaAdministracao;

    @Column(name = "VALOR_VENDA")
    private BigDecimal valorVenda;

    @Column(name = "AREA_CLIENTE", length = 50)
    private String areaCliente;

    @Column(name = "CHAVE_ERP", length = 50)
    private String chaveErp;

    @Column(name = "DTHR_RETORNO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtHrRetorno;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

    public String getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(String autorizacao) {
        this.autorizacao = autorizacao;
    }

    public Integer getBanco() {
        return banco;
    }

    public void setBanco(Integer banco) {
        this.banco = banco;
    }

    public Integer getBandeira() {
        return bandeira;
    }

    public void setBandeira(Integer bandeira) {
        this.bandeira = bandeira;
    }

    public Long getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(Long codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    public Integer getCodigoLojaErp() {
        return codigoLojaErp;
    }

    public void setCodigoLojaErp(Integer codigoLojaErp) {
        this.codigoLojaErp = codigoLojaErp;
    }

    public Integer getConta() {
        return conta;
    }

    public void setConta(Integer conta) {
        this.conta = conta;
    }

    public Date getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public Long getNsu() {
        return nsu;
    }

    public void setNsu(Long nsu) {
        this.nsu = nsu;
    }

    public Integer getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(Integer numeroLote) {
        this.numeroLote = numeroLote;
    }

    public Integer getPlano() {
        return plano;
    }

    public void setPlano(Integer plano) {
        this.plano = plano;
    }

    public Character getProduto() {
        return produto;
    }

    public void setProduto(Character produto) {
        this.produto = produto;
    }

    public Integer getRede() {
        return rede;
    }

    public void setRede(Integer rede) {
        this.rede = rede;
    }

    public Integer getStatusConciliacao() {
        return statusConciliacao;
    }

    public void setStatusConciliacao(Integer statusConciliacao) {
        this.statusConciliacao = statusConciliacao;
    }

    public Date getDataCredito() {
        return dataCredito;
    }

    public void setDataCredito(Date dataCredito) {
        this.dataCredito = dataCredito;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public BigDecimal getTaxaAdministracao() {
        return taxaAdministracao;
    }

    public void setTaxaAdministracao(BigDecimal taxaAdministracao) {
        this.taxaAdministracao = taxaAdministracao;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    public String getAreaCliente() {
        return areaCliente;
    }

    public void setAreaCliente(String areaCliente) {
        this.areaCliente = areaCliente;
    }

    public String getChaveErp() {
        return chaveErp;
    }

    public void setChaveErp(String chaveErp) {
        this.chaveErp = chaveErp;
    }

    public Date getDtHrRetorno() {
        return dtHrRetorno;
    }

    public void setDtHrRetorno(Date dtHrRetorno) {
        this.dtHrRetorno = dtHrRetorno;
    }

}
