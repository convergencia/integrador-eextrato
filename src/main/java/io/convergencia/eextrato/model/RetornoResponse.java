/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RetornoResponse")
public class RetornoResponse {

    private Erro erro;

    private Integer paginaAtual;
    private Integer totalPaginas;

    private Collection<RetornoVenda> vendas;
    private Collection<RetornoPagamento> pagamentos;
    private Collection<RetornoAjusteTarifa> ajustes;

    public Erro getErro() {
        return erro;
    }

    public void setErro(Erro erro) {
        this.erro = erro;
    }

    public Integer getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(Integer paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    @XmlElementWrapper(name = "vendas")
    @XmlElement(name = "venda")
    public Collection<RetornoVenda> getVendas() {
        if (vendas == null) {
            vendas = new ArrayList<>();
        }
        return vendas;
    }

    public void setVendas(Collection<RetornoVenda> vendas) {
        this.vendas = vendas;
    }

    @XmlElementWrapper(name = "pagamentos")
    @XmlElement(name = "pagamento")
    public Collection<RetornoPagamento> getPagamentos() {
        if (pagamentos == null) {
            pagamentos = new ArrayList<>();
        }
        return pagamentos;
    }

    public void setPagamentos(Collection<RetornoPagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }

    @XmlElementWrapper(name = "ajustes")
    @XmlElement(name = "ajuste")
    public Collection<RetornoAjusteTarifa> getAjustes() {
        if (ajustes == null) {
            ajustes = new ArrayList<>();
        }
        return ajustes;
    }

    public void setAjustes(Collection<RetornoAjusteTarifa> ajustes) {
        this.ajustes = ajustes;
    }

}
