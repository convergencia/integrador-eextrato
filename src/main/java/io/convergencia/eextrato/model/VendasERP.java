package io.convergencia.eextrato.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "VendasERP")
public class VendasERP {    
    
    Collection<EnvioVenda> vendas;

    @XmlElement(name = "venda")
    public Collection<EnvioVenda> getVendas() {
        if(vendas == null){
            vendas = new ArrayList<>();
        }
        return vendas;
    }

    public void setVendas(Collection<EnvioVenda> vendas) {
        this.vendas = vendas;
    }

    public boolean add(EnvioVenda e) {
        return getVendas().add(e);
    }
    
}
