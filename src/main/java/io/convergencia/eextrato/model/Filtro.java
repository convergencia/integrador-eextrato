package io.convergencia.eextrato.model;

import io.convergencia.eextrato.api.DateConvert;
import java.util.Collection;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "Filtro")
public class Filtro {

    private Date dataVendaInicio;
    private Date dataVendaFim;
    private Date dataPagamentoInicio;
    private Date dataPagamentoFim;
    private Integer tipoArquivo;
    private Integer tipoVenda;
    private String dadosCliente;
    private Integer pagina;
    private Collection<Integer> rede;
    private Collection<Integer> bandeira;

    @XmlJavaTypeAdapter(DateConvert.class)
    public Date getDataVendaInicio() {
        return dataVendaInicio;
    }

    public void setDataVendaInicio(Date dataVendaInicio) {
        this.dataVendaInicio = dataVendaInicio;
    }

    @XmlJavaTypeAdapter(DateConvert.class)
    public Date getDataVendaFim() {
        return dataVendaFim;
    }

    public void setDataVendaFim(Date dataVendaFim) {
        this.dataVendaFim = dataVendaFim;
    }

    @XmlJavaTypeAdapter(DateConvert.class)
    public Date getDataPagamentoInicio() {
        return dataPagamentoInicio;
    }

    public void setDataPagamentoInicio(Date dataPagamentoInicio) {
        this.dataPagamentoInicio = dataPagamentoInicio;
    }

    @XmlJavaTypeAdapter(DateConvert.class)
    public Date getDataPagamentoFim() {
        return dataPagamentoFim;
    }

    public void setDataPagamentoFim(Date dataPagamentoFim) {
        this.dataPagamentoFim = dataPagamentoFim;
    }

    public Integer getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(Integer tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public Integer getTipoVenda() {
        return tipoVenda;
    }

    public void setTipoVenda(Integer tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    public String getDadosCliente() {
        return dadosCliente;
    }

    public void setDadosCliente(String dadosCliente) {
        this.dadosCliente = dadosCliente;
    }

    public Integer getPagina() {
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina = pagina;
    }

    public Collection<Integer> getRede() {
        return rede;
    }

    public void setRede(Collection<Integer> rede) {
        this.rede = rede;
    }

    public Collection<Integer> getBandeira() {
        return bandeira;
    }

    public void setBandeira(Collection<Integer> bandeira) {
        this.bandeira = bandeira;
    }

}
