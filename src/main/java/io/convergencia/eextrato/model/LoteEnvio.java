/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.model;

import io.convergencia.eextrato.api.enuns.EStatusLote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author wanderson
 */
@Entity(name = "INTEGRA_CONC_TEF_ENV_LOTE")
@DynamicUpdate
public class LoteEnvio extends Entidade {

    @Id
    @Column(name = "ID")
    private String id;

    @OneToMany(mappedBy = "loteEnvio", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<LoteEnvioVenda> vendas;

    @Column(name = "DATA_CRIACAO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date data;

    @Column(name = "STATUS_VENDA")
    @Enumerated
    private EStatusLote statusVenda = EStatusLote.CRIADO;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @PrePersist
    private void pre() {
        this.id = UUID.randomUUID().toString().toUpperCase();
        this.data = new Date();
    }

    public Collection<LoteEnvioVenda> getVendas() {
        if (null == vendas) {
            vendas = new ArrayList<>();
        }
        return vendas;
    }

    public LoteEnvioVenda getVendaById(Long id) {

        for (LoteEnvioVenda t : getVendas()) {
            Long thisId = t.getId();
            if (thisId != null && thisId.equals(id)) {
                return t;
            }
        }

        return null;

    }

    public LoteEnvioVenda getVendaByAreaCliente(String areaCliente) {

        for (LoteEnvioVenda t : getVendas()) {
            String thisAreaCliente = t.getVenda().getAreaCliente();
            if (areaCliente != null && thisAreaCliente.equals(areaCliente)) {
                return t;
            }
        }

        return null;

    }

    public LoteEnvioVenda getVendaByEnvio(EnvioVenda ev) {

        LoteEnvioVenda lev = new LoteEnvioVenda();
        lev.setVenda(ev);
        if (getVendas().contains(lev)) {
            List<LoteEnvioVenda> levs = (List<LoteEnvioVenda>) getVendas();
            return levs.get(levs.indexOf(lev));
        }

        return null;
    }

    public void setVendas(Collection<LoteEnvioVenda> vendas) {
        this.vendas = vendas;
    }

    public EStatusLote getStatusVenda() {

        return statusVenda;
    }

    public void setStatusVenda(EStatusLote statusVenda) {
        this.statusVenda = statusVenda;
    }

    public boolean add(LoteEnvioVenda e) {

        return getVendas().add(e);
    }

}
