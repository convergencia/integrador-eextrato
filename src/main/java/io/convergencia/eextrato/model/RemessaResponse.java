package io.convergencia.eextrato.model;

import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RemessaResponse")
public class RemessaResponse {

    private Erro erro;
    private String status;
    private Collection<VendaERPErro> vendaERPErro;

    public Erro getErro() {
        return erro;
    }

    public void setErro(Erro erro) {
        this.erro = erro;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlElement(name = "vendaERPErro")
    public Collection<VendaERPErro> getVendaERPErro() {
        return vendaERPErro;
    }

    public void setVendaERPErro(Collection<VendaERPErro> vendaERPErro) {
        this.vendaERPErro = vendaERPErro;
    }
    
    

}
