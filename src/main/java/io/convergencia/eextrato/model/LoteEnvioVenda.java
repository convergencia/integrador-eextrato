/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.model;

import io.convergencia.eextrato.api.enuns.EStatusLote;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author wanderson
 */
@Entity(name = "INTEGRA_CONC_TEF_ENV_LOTE_VENDA")
@DynamicUpdate
public class LoteEnvioVenda extends Entidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    @JoinColumn(name = "LOTE_ENVIO_ID")
    @ManyToOne
    private LoteEnvio loteEnvio;

    @OneToOne
    @JoinColumn(name = "ENVIO_VENDA_ID")
    private EnvioVenda venda;

    @Column(name = "STATUS_VENDA")
    @Enumerated
    private EStatusLote statusVenda = EStatusLote.CRIADO;

    public LoteEnvioVenda() {
    }

    public LoteEnvioVenda(LoteEnvio loteEnvio, EnvioVenda venda) {
        this.loteEnvio = loteEnvio;
        this.venda = venda;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoteEnvio getLoteEnvio() {
        return loteEnvio;
    }

    public void setLoteEnvio(LoteEnvio loteEnvio) {
        this.loteEnvio = loteEnvio;
    }

    public EnvioVenda getVenda() {
        return venda;
    }

    public void setVenda(EnvioVenda venda) {
        this.venda = venda;
    }

    public EStatusLote getStatusVenda() {
        return statusVenda;
    }

    public void setStatusVenda(EStatusLote statusVenda) {
        this.statusVenda = statusVenda;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.venda);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LoteEnvioVenda other = (LoteEnvioVenda) obj;
        return Objects.equals(this.venda, other.venda);
    }
    
    

}
