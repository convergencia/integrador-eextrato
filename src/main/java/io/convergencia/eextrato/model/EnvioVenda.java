package io.convergencia.eextrato.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.convergencia.eextrato.api.DateConvert;
import io.convergencia.eextrato.api.MoneyConvert;
import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.hibernate.annotations.DynamicUpdate;

@Entity(name = "INTEGRA_CONC_TEF_ENV_VENDA")
@DynamicUpdate
@XmlRootElement(name = "venda")
public class EnvioVenda extends Entidade {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COD_VENDA")
    private Integer codigoVenda;

    @Column(name = "AUTORIZACAO")
    private String autorizacao;

    @Column(name = "COD_ESTABELECIMENTO")
    private String codigoEstabelecimento;

    @Column(name = "DATA_VENDA")
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date dataVenda;

    @Column(name = "NSU")
    private String nsu;

    @Column(name = "NSU_HOST")
    private String nsuHost;

    @Column(name = "PLANO")
    private Integer plano;

    @Column(name = "PRODUTO")
    private String produto;

    @Column(name = "REDE")
    private Integer rede;
    
    @Column(name = "BANDEIRA")
    private Integer bandeira;

    @Column(name = "VALOR")
    private BigDecimal valor;

    @Column(name = "COD_LOJA")
    private Integer codigoLoja;

    @Column(name = "CHAVE_ERP", length = 50)
    private String chaveErp;

    @Column(name = "CAMPO_LIVRE", length = 50)
    private String areaCliente;

    @Column(name = "STATUS_CONCILIACAO")
    @Enumerated
    private EEnvioEvento status;

    @XmlTransient
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodigoVenda() {
        return codigoVenda;
    }

    public void setCodigoVenda(Integer codigoVenda) {
        this.codigoVenda = codigoVenda;
    }

    public String getAutorizacao() {
        return autorizacao;
    }

    public void setAutorizacao(String autorizacao) {
        this.autorizacao = autorizacao;
    }

    public String getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(String codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    @XmlJavaTypeAdapter(DateConvert.class)
    public Date getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public String getNsu() {
        return nsu;
    }

    public void setNsu(String nsu) {
        this.nsu = nsu;
    }

    public String getNsuHost() {
        return nsuHost;
    }

    public void setNsuHost(String nsuHost) {
        this.nsuHost = nsuHost;
    }

    public Integer getPlano() {
        return plano;
    }

    public void setPlano(Integer plano) {
        this.plano = plano;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public Integer getRede() {
        return rede;
    }

    public void setRede(Integer rede) {
        this.rede = rede;
    }

    @XmlJavaTypeAdapter(MoneyConvert.class)
    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getCodigoLoja() {
        return codigoLoja;
    }

    public void setCodigoLoja(Integer codigoLoja) {
        this.codigoLoja = codigoLoja;
    }

    public String getAreaCliente() {
        return areaCliente;
    }

    public void setAreaCliente(String areaCliente) {
        this.areaCliente = areaCliente;
    }

    @XmlTransient
    public EEnvioEvento getStatus() {
        return status;
    }

    public void setStatus(EEnvioEvento status) {
        this.status = status;
    }

    public String getChaveErp() {
        return chaveErp;
    }

    @XmlTransient
    public void setChaveErp(String chaveErp) {
        this.chaveErp = chaveErp;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EnvioVenda other = (EnvioVenda) obj;
        return Objects.equals(this.id, other.id);
    }

}
