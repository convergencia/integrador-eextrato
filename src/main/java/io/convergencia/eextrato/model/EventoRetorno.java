package io.convergencia.eextrato.model;

import io.convergencia.eextrato.api.enuns.ERetornoEvento;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.DynamicUpdate;

@Entity(name = "INTEGRA_CONC_TEF_RET_EVENTO")
@DynamicUpdate
public class EventoRetorno extends Entidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlTransient
    @Column(name = "ID")
    private Long id;

    @Column(name = "PROCESSAR_DATA")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date processarData;

    @Column(name = "TIPO_ARQUIVO")
    private Integer tipoArquivo;

    @Column(name = "PAGINA_ATUAL")
    private Integer paginaAtual = 1;

    @Column(name = "TOTAL_PAGINAS")
    private Integer totalPaginas = 1;

    @Column(name = "INICIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date inicio;

    @Column(name = "ULTIMA_EXECUCAO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date ultimaExecucao;

    @Column(name = "FIM")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fim;

    @Column(name = "STATUS")
    @Enumerated
    private ERetornoEvento status = ERetornoEvento.ABERTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getProcessarData() {
        return processarData;
    }

    public void setProcessarData(Date processarData) {
        this.processarData = processarData;
    }

    public Integer getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(Integer tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public Integer getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(Integer paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getUltimaExecucao() {
        return ultimaExecucao;
    }

    public void setUltimaExecucao(Date ultimaExecucao) {
        this.ultimaExecucao = ultimaExecucao;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public ERetornoEvento getStatus() {
        return status;
    }

    public void setStatus(ERetornoEvento status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.processarData);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventoRetorno other = (EventoRetorno) obj;
        if (!Objects.equals(this.processarData, other.processarData)) {
            return false;
        }
        return true;
    }

}
