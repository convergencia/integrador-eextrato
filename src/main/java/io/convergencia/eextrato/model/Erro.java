package io.convergencia.eextrato.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "erro")
public class Erro {

    private String descricao;
    private Boolean rejeitado;
    private String status;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getRejeitado() {
        return rejeitado;
    }

    public void setRejeitado(Boolean rejeitado) {
        this.rejeitado = rejeitado;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" + "descricao:'" + descricao + "', rejeitado:'" + rejeitado + "', status:'" + status + "'}";
    }
    
    

}
