/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.model;

import io.convergencia.eextrato.api.enuns.EStatus;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import org.hibernate.annotations.DynamicUpdate;

@Entity(name = "INTEGRA_CONC_TEF_LOJA")
@DynamicUpdate
public class LojaERP extends Entidade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COD_LOJA")
    private Integer codigoLoja;

    @Column(name = "NOME_LOJA")
    private String nomeLoja;

    @Column(name = "STATUS")
    @Enumerated
    private EStatus status = EStatus.ATIVO;

    @Column(name = "DATA_CAD")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "DATA_ALT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodigoLoja() {
        return codigoLoja;
    }

    public void setCodigoLoja(Integer codigoLoja) {
        this.codigoLoja = codigoLoja;
    }

    public String getNomeLoja() {
        return nomeLoja;
    }

    public void setNomeLoja(String nomeLoja) {
        this.nomeLoja = nomeLoja;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(EStatus status) {
        this.status = status;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    @PrePersist
    void onCreate() {
        this.setDataCadastro(Calendar.getInstance().getTime());
    }

    @PreUpdate
    void onPersist() {
        this.setDataAlteracao(Calendar.getInstance().getTime());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.codigoLoja);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LojaERP other = (LojaERP) obj;
        return Objects.equals(this.codigoLoja, other.codigoLoja);
    }

}
