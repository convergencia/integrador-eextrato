/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.params;

import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Wanderson
 */
public class ParamSearch {

    private Date periodoIni;
    private Date periodoFim;
    private Long loteIni;
    private Long loteFim;
    private Collection<Integer> redes;
    private Collection<Integer> bandeiras;
    private Collection<Integer> codigoLojas;
    private String status;

    public Date getPeriodoIni() {
        return periodoIni;
    }

    public void setPeriodoIni(Date periodoIni) {
        this.periodoIni = periodoIni;
    }

    public Date getPeriodoFim() {
        return periodoFim;
    }

    public void setPeriodoFim(Date periodoFim) {
        this.periodoFim = periodoFim;
    }

    public Long getLoteIni() {
        return loteIni;
    }

    public void setLoteIni(Long loteIni) {
        this.loteIni = loteIni;
    }

    public Long getLoteFim() {
        return loteFim;
    }

    public void setLoteFim(Long loteFim) {
        this.loteFim = loteFim;
    }

    public Collection<Integer> getRedes() {
        return redes;
    }

    public void setRedes(Collection<Integer> redes) {
        this.redes = redes;
    }

    public Collection<Integer> getBandeiras() {
        return bandeiras;
    }

    public void setBandeiras(Collection<Integer> bandeiras) {
        this.bandeiras = bandeiras;
    }

    public Collection<Integer> getCodigoLojas() {
        return codigoLojas;
    }

    public void setCodigoLojas(Collection<Integer> codigoLojas) {
        this.codigoLojas = codigoLojas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
