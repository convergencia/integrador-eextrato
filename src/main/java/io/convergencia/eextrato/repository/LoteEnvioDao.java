/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.api.enuns.EStatusLote;
import io.convergencia.eextrato.model.LoteEnvio;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author wanderson
 */
@RepositoryRestResource(collectionResourceRel = "lotes", path = "lotes")
public interface LoteEnvioDao extends PagingAndSortingRepository<LoteEnvio, String>, JpaSpecificationExecutor<LoteEnvio> {

    List<LoteEnvio> findByStatusVenda(EStatusLote l);

    List<LoteEnvio> findTop100ByStatusVendaInOrderByDataAsc(EStatusLote... l);
}
