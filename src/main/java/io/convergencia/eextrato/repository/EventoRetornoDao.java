/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.model.EventoRetorno;
import java.util.Date;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "eventos", path = "eventos")
public interface EventoRetornoDao extends PagingAndSortingRepository<EventoRetorno, Long> {


    public EventoRetorno findByProcessarDataAndTipoArquivo(@Param("processarData") Date d, @Param("tipoArquivo") Integer t);

}
