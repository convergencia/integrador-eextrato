/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.model.RetornoVenda;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "retornovendas", path = "retornovendas")
public interface RetornoVendaDao extends PagingAndSortingRepository<RetornoVenda, Long>, JpaSpecificationExecutor<RetornoVenda>{
    
    @Procedure(value = "P_INTEGRA_CONC_TEF_RET_VENDA")
    void processaRetornoErp();
    
}
