/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.model.ErroSistema;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.web.PageableDefault;

/**
 *
 * @author Wanderson
 */
@RepositoryRestResource(collectionResourceRel = "erro-sistema", path = "erro-sistema")
public interface ErroSistemaDao extends PagingAndSortingRepository<ErroSistema, Long> {

    Page<ErroSistema> findByOrderByIdDesc(@PageableDefault Pageable p);

}
