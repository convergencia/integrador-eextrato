/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.model.LoteEnvioVenda;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author wanderson
 */
@RepositoryRestResource(collectionResourceRel = "loteEnvioVendas", path = "loteEnvioVendas")
public interface LoteEnvioVendaDao extends PagingAndSortingRepository<LoteEnvioVenda, Long>, JpaSpecificationExecutor<LoteEnvioVenda> {

}
