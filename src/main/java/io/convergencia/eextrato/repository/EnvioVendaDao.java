/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.api.enuns.EEnvioEvento;
import io.convergencia.eextrato.model.EnvioVenda;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "vendas", path = "vendas")
public interface EnvioVendaDao extends PagingAndSortingRepository<EnvioVenda, Long>, JpaSpecificationExecutor<EnvioVenda> {

    List<EnvioVenda> findTop100ByStatus(EEnvioEvento ee);

    EnvioVenda findByCodigoLojaAndNsu(Integer codigoLoja, String nsu);

    Long countByStatus(EEnvioEvento ee);

    @Procedure(value = "P_INTEGRA_CONC_TEF_ENV_VENDA")
    void addPendencia();

}
