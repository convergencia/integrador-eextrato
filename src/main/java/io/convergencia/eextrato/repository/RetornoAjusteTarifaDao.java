/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.repository;

import io.convergencia.eextrato.model.RetornoAjusteTarifa;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "ajustes", path = "ajustes")
public interface RetornoAjusteTarifaDao extends PagingAndSortingRepository<RetornoAjusteTarifa, Long>, JpaSpecificationExecutor<RetornoAjusteTarifa> {

    @Procedure(value = "P_INTEGRA_CONC_TEF_RET_AJUSTE_TARIFAS")
    void processaRetornoErp();
}
