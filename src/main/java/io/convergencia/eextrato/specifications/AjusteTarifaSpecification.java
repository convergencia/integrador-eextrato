/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.eextrato.specifications;

import io.convergencia.eextrato.model.RetornoAjusteTarifa;
import io.convergencia.eextrato.params.ParamSearch;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author Wanderson
 */
public class AjusteTarifaSpecification {

    public static Specification<RetornoAjusteTarifa> searchPagamento(ParamSearch ps) {
        return (Root<RetornoAjusteTarifa> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (ps.getPeriodoIni() != null && ps.getPeriodoFim() != null) {
                predicates.add(builder.between(root.<Date>get("dataVenda"), ps.getPeriodoIni(), ps.getPeriodoFim()));
            } else {
                if (ps.getPeriodoIni() != null) {
                    predicates.add(builder.between(root.<Date>get("dataVenda"), ps.getPeriodoIni(), ps.getPeriodoIni()));
                }
            }

            if (ps.getLoteIni() != null && ps.getLoteIni() > 0 && ps.getLoteFim() != null && ps.getLoteFim() > 0) {
                predicates.add(builder.between(root.<Long>get("numeroLote"), ps.getLoteIni(), ps.getLoteFim()));
            } else {
                if (ps.getLoteIni() != null && ps.getLoteIni() > 0) {
                    predicates.add(builder.between(root.<Long>get("numeroLote"), ps.getLoteIni(), ps.getLoteIni()));
                }
            }

            if (ps.getRedes() != null && !ps.getRedes().isEmpty()) {
                predicates.add(root.<Integer>get("rede").in(ps.getRedes()));
            }

            if (ps.getBandeiras() != null && !ps.getBandeiras().isEmpty()) {
                predicates.add(root.<Integer>get("bandeira").in(ps.getBandeiras()));
            }

            if (ps.getCodigoLojas() != null && !ps.getCodigoLojas().isEmpty()) {
                predicates.add(root.<Integer>get("codigoLojaErp").in(ps.getCodigoLojas()));
            }
            return builder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
