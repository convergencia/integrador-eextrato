var gulp = require('gulp'),
        usemin = require('gulp-usemin'),
        wrap = require('gulp-wrap'),
        connect = require('gulp-connect'),
        watch = require('gulp-watch'),
        minifyCss = require('gulp-cssnano'),
        minifyJs = require('gulp-uglify'),
        concat = require('gulp-concat'),
        less = require('gulp-less'),
        rename = require('gulp-rename'),
        minifyHTML = require('gulp-htmlmin'),
        jshint = require('gulp-jshint');

var paths = {
    scripts: 'src/main/resources/static/js/**/*.*',
    styles: 'src/main/resources/static/less/**/*.*',
    images: 'src/main/resources/static/img/**/*.*',
    templates: 'src/main/resources/static/templates/**/*.html',
    index: 'src/main/resources/static/index.html',
    bower_fonts: 'src/main/resources/static/components/**/*.{ttf,woff,eof,svg}',
    scripts_oauth: 'src/main/resources/static/oauth/**/*.*'
};

gulp.task('jshint', function () {
    return gulp.src(paths.scripts)
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
});



/**
 * Handle bower components from index
 */
gulp.task('usemin', function () {
    return gulp.src(paths.index)
            .pipe(usemin({
                js: ['concat'],
                css: [minifyCss({keepSpecialComments: 0}), 'concat']
            }))
            .pipe(gulp.dest('src/main/resources/resources/'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-bower_fonts']);

gulp.task('copy-bower_fonts', function () {
    return gulp.src(paths.bower_fonts)
            .pipe(rename({
                dirname: '/fonts'
            }))
            .pipe(gulp.dest('src/main/resources/resources/lib'));
});

/**
 * Handle custom files
 */
gulp.task('build-custom', ['custom-images', 'custom-js', 'custom-js-oauth', 'custom-less', 'custom-templates']);

gulp.task('custom-images', function () {
    return gulp.src(paths.images)
            .pipe(gulp.dest('src/main/resources/resources/img'));
});

gulp.task('custom-js', function () {
    return gulp.src(paths.scripts)
            //.pipe(minifyJs())
            .pipe(concat('dashboard.min.js'))
            .pipe(gulp.dest('src/main/resources/resources/js'));
});

gulp.task('custom-js-oauth', function () {
    return gulp.src(paths.scripts_oauth)
            .pipe(gulp.dest('src/main/resources/resources/js/oauth'));
});

gulp.task('custom-less', function () {
    return gulp.src(paths.styles)
            .pipe(less())
            .pipe(gulp.dest('src/main/resources/resources/css'));
});

gulp.task('custom-templates', function () {
    return gulp.src(paths.templates)
            .pipe(minifyHTML({collapseWhitespace: true}))
            .pipe(gulp.dest('src/main/resources/resources/templates'));
});

/**
 * Watch custom files
 */
gulp.task('watch', function () {
    gulp.watch([paths.images], ['custom-images']);
    gulp.watch([paths.styles], ['custom-less']);
    gulp.watch([paths.scripts], ['custom-js']);
    gulp.watch([paths.templates], ['custom-templates']);
    gulp.watch([paths.index], ['usemin']);
});

/**
 * Live reload server
 */
gulp.task('webserver', function () {
    connect.server({
        root: 'src/main/resources/resources',
        livereload: true,
        port: 8888
    });
});

gulp.task('livereload', function () {
    gulp.src(['src/main/resources/resources/**/*.*'])
            .pipe(watch(['src/main/resources/resources/**/*.*']))
            .pipe(connect.reload());
});

/**
 * Gulp tasks
 */
gulp.task('build', ['jshint', 'usemin', 'build-assets', 'build-custom']);
gulp.task('default', ['build', 'webserver', 'livereload', 'watch']);